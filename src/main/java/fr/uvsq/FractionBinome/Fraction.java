package fr.uvsq.FractionBinome;

public class Fraction {
	private final int numerateur;
	private final int denominateur;

	public Fraction() {
		this.numerateur = 0;
		this.denominateur = 1;
	}

	public Fraction(int num) {
		this.numerateur = num;
		this.denominateur = 1;
	}

	public Fraction(int num, int den) throws IllegalArgumentException {
		this.numerateur = num;
		if (den == 0)
			throw new IllegalArgumentException();
		this.denominateur = den;
	}

	public int consultNum() {
		return numerateur;
	}

	public int consultDen() {
		return denominateur;
	}

	public static final Fraction ZERO = new Fraction(0, 1);
	public static final Fraction UN = new Fraction(1, 1);
	
	public String toString(){
		return Integer.toString(numerateur) + "/" + Integer.toString(denominateur);
	}

	public double result() {
		return (double) numerateur / (double) denominateur;
	}
}
