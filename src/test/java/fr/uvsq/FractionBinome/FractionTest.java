package fr.uvsq.FractionBinome;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FractionTest {

	@Test
	public void construct1() {
		Fraction f = new Fraction();
		assertEquals(0, f.consultNum());
		assertEquals(1, f.consultDen());
	}

	@Test
	public void construct2() {
		Fraction f = new Fraction(2);
		assertEquals(2, f.consultNum());
		assertEquals(1, f.consultDen());
	}

	@Test
	public void construct3() {
		Fraction f = new Fraction(3, 4);
		assertEquals(3, f.consultNum());
		assertEquals(4, f.consultDen());
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructFail() {
		Fraction f = new Fraction(1, 0);
	}

	@Test
	public void constantZERO() {
		Fraction F = Fraction.ZERO;
		assertEquals(0, F.consultNum());
		assertEquals(1, F.consultDen());
	}

	@Test
	public void constantUN() {
		Fraction F = Fraction.UN;
		assertEquals(1, F.consultNum());
		assertEquals(1, F.consultDen());
	}

	@Test
	public void testResult() {
		Fraction F = new Fraction(5, 2);
		assertEquals(2.5, F.result(), 0);
	}
	
	@Test
	public void toStringTest(){
		Fraction F = Fraction.UN;
		assertEquals("1/1", F.toString());
	}
}
